<div class="row">
    <a href="#" id="jsPromoTop" data-promo="promo1" class="promo promo__top" title="Promo (Top)" tabindex="4"><span class="sr-only translate" data-translate="PromoTop">Promo Top</span></a>
</div>

<div class="row">
    <a href="#" id="jsPromoBottom" data-promo="promo2" class="promo promo__bottom" title="Promo (Bottom)" tabindex="4"><span class="sr-only translate" data-translate="PromoBottom">Promo Bottom</span></a>
</div>
